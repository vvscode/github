import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';

export default Model.extend({
  issue: belongsTo('ussue'),
  url: attr(),
  html_url: attr(),
  body: attr(),
  user: attr(),
  createdAt: attr('date'),
  updatedAt: attr('date')
});
