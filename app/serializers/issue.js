import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  primaryKey: 'number',
  normalize(modelClass, responseHash, prop) {
    responseHash.links = {
      comments: `${responseHash.comments_url}`
    };

    responseHash.commentsCount = responseHash.comments;
    delete responseHash.comments;

    return this._super(modelClass, responseHash, prop);
  }
});
